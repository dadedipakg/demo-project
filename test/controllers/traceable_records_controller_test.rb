require 'test_helper'

class TraceableRecordsControllerTest < ActionController::TestCase
  setup do
    @traceable_record = traceable_records(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:traceable_records)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create traceable_record" do
    assert_difference('TraceableRecord.count') do
      post :create, traceable_record: { commas1_id,: @traceable_record.commas1_id,, custom_id: @traceable_record.custom_id, customer,: @traceable_record.customer,, date_shipped,: @traceable_record.date_shipped,, description,: @traceable_record.description,, installed_software_version,: @traceable_record.installed_software_version,, ip_address: @traceable_record.ip_address, item,: @traceable_record.item,, mfg_number,: @traceable_record.mfg_number,, mfg_serial_number,: @traceable_record.mfg_serial_number,, sales_order,: @traceable_record.sales_order,, sim_id,: @traceable_record.sim_id, }
    end

    assert_redirected_to traceable_record_path(assigns(:traceable_record))
  end

  test "should show traceable_record" do
    get :show, id: @traceable_record
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @traceable_record
    assert_response :success
  end

  test "should update traceable_record" do
    patch :update, id: @traceable_record, traceable_record: { commas1_id,: @traceable_record.commas1_id,, custom_id: @traceable_record.custom_id, customer,: @traceable_record.customer,, date_shipped,: @traceable_record.date_shipped,, description,: @traceable_record.description,, installed_software_version,: @traceable_record.installed_software_version,, ip_address: @traceable_record.ip_address, item,: @traceable_record.item,, mfg_number,: @traceable_record.mfg_number,, mfg_serial_number,: @traceable_record.mfg_serial_number,, sales_order,: @traceable_record.sales_order,, sim_id,: @traceable_record.sim_id, }
    assert_redirected_to traceable_record_path(assigns(:traceable_record))
  end

  test "should destroy traceable_record" do
    assert_difference('TraceableRecord.count', -1) do
      delete :destroy, id: @traceable_record
    end

    assert_redirected_to traceable_records_path
  end
end
