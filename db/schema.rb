# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_30_142629) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "trace_records", force: :cascade do |t|
    t.string "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "traceable_records", force: :cascade do |t|
    t.string "custom_id"
    t.string "sales_order"
    t.string "customer"
    t.string "item"
    t.string "description"
    t.string "date_shipped"
    t.string "mfg_number"
    t.string "mfg_serial_number"
    t.string "commas1_id"
    t.string "installed_software_version"
    t.string "sim_id"
    t.string "ip_address"
    t.string "file_name"
    t.string "file_upload_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
