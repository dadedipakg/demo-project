class CreateTraceableRecords < ActiveRecord::Migration[5.1]
  def change
    create_table :traceable_records do |t|
      t.string :custom_id
      t.string :sales_order
      t.string :customer
      t.string :item
      t.string :description
      t.string :date_shipped
      t.string :mfg_number
      t.string :mfg_serial_number
      t.string :commas1_id
      t.string :installed_software_version
      t.string :sim_id
      t.string :ip_address
      t.string :file_name
      t.string :file_upload_date
      t.timestamps null: false
    end
  end
end
