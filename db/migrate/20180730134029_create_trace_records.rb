class CreateTraceRecords < ActiveRecord::Migration[5.1]
  def change
    create_table :trace_records do |t|
      t.string :file
      t.timestamps null: false
    end
  end
end
