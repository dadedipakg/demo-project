class TraceableRecordsController < ApplicationController
  before_action :set_traceable_record, only: [:show, :edit, :update, :destroy]

  # GET /traceable_records
  # GET /traceable_records.json
  def index
    @traceable_records = TraceableRecord.all
  end

  # GET /traceable_records/1
  # GET /traceable_records/1.json
  def show
  end

  # GET /traceable_records/new
  def new
    @traceable_record = TraceableRecord.new
  end

  # GET /traceable_records/1/edit
  def edit
  end

  # POST /traceable_records
  # POST /traceable_records.json
  def create
    @traceable_record = TraceableRecord.new(traceable_record_params)

    respond_to do |format|
      if @traceable_record.save
        format.html { redirect_to @traceable_record, notice: 'Traceable record was successfully created.' }
        format.json { render :show, status: :created, location: @traceable_record }
      else
        format.html { render :new }
        format.json { render json: @traceable_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /traceable_records/1
  # PATCH/PUT /traceable_records/1.json
  def update
    respond_to do |format|
      if @traceable_record.update(traceable_record_params)
        format.html { redirect_to @traceable_record, notice: 'Traceable record was successfully updated.' }
        format.json { render :show, status: :ok, location: @traceable_record }
      else
        format.html { render :edit }
        format.json { render json: @traceable_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /traceable_records/1
  # DELETE /traceable_records/1.json
  def destroy
    @traceable_record.destroy
    respond_to do |format|
      format.html { redirect_to traceable_records_url, notice: 'Traceable record was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import
    @trace_record = TraceRecord.create(import_params)
    xml = File.open(@trace_record.file.path)
    data = Hash.from_xml(xml)
    rows = data['Workbook']['Worksheet']['Table']['Row']
    rows.each do |row|
      next if (rows[0] == row) || row['Cell'].count != 12
      values = row['Cell'].collect{|a| a['Data']}
      traceable_record = TraceableRecord.new
      columns.each_with_index do |column, index|
        traceable_record.send("#{column}=",values[index])
      end
      traceable_record.file_name =  @trace_record.file.filename
      traceable_record.file_upload_date = Date.today.to_s
      traceable_record.save
    end
    redirect_to root_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_traceable_record
      @traceable_record = TraceableRecord.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def traceable_record_params
      params.require(:traceable_record).permit(:custom_id, :sales_order, :customer, :item, :description, :date_shipped, :mfg_number, :mfg_serial_number, :commas1_id, :installed_software_version, :sim_id, :ip_address)
    end

    def columns
      ['custom_id', 'sales_order', 'customer', 'item', 'description', 'date_shipped', 'mfg_number', 'mfg_serial_number', 'commas1_id', 'installed_software_version', 'sim_id', 'ip_address']
    end

    def import_params
      params.require(:trace_record).permit!
    end
end
