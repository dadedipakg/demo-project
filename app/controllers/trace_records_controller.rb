class TraceRecordsController < ApplicationController

	def create
		
		@trace_record = TraceRecord.create(permitted_params)
		byebug
		column_names = data['Workbook']['Worksheet']['Table']['Row'][0]['Cell'].collect{|a| a['Data'].gsub(' ', '_').gsub('-', '').downcase}
		ActiveRecord::Base.connection.create_table :traceable_records do |t|
			column_names.each do |column_name|
		    t.string column_name
		    t.text :body
		  end
		redirect_to root_path
	end

	def permitted_params
		params.require(:trace_record).permit!
	end

end
