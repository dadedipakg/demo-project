json.extract! traceable_record, :id, :custom_id, :sales_order, :customer, :item, :description, :date_shipped, :mfg_number, :mfg_serial_number, :commas1_id, :installed_software_version, :sim_id, :ip_address, :created_at, :updated_at, :file_upload_date, :file_name
json.url traceable_record_url(traceable_record, format: :json)
